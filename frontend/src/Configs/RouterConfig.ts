import { createBrowserRouter, redirect } from "react-router-dom";
import Layout from '../Layouts/Layout';
import PublicPage from '../Pages/PublicPage';
import LoginPage from '../Auth/LoginPage';
import {ProtectedPage} from '../Pages/ProtectedPage';
import {fakeAuthProvider, loginAction, loginLoader, protectedLoader} from "../Auth/Auth";

export const router = createBrowserRouter([
    {
        id: "root",
        path: "/",
        loader() {
            // Our root route always provides the user, if logged in
            return { user: fakeAuthProvider.username };
        },
        Component: Layout,
        children: [
            {
                index: true,
                Component: PublicPage,
            },
            {
                path: "login",
                action: loginAction,
                loader: loginLoader,
                Component: LoginPage,
            },
            {
                path: "protected",
                loader: protectedLoader,
                Component: ProtectedPage,
            },
        ],
    },
    {
        path: "/logout",
        async action() {
            // We signout in a "resource route" that we can hit from a fetcher.Form
            await fakeAuthProvider.signout();
            return redirect("/");
        },
    },
]);

export default router;
