import React from 'react';
import { RouterProvider } from "react-router-dom";
import router from './Configs/RouterConfig';

export default function App() {
    return (
        <RouterProvider router={router} fallbackElement={<p>Initial Load...</p>} />
    );
}
