import * as process from 'process';

export default () => ({
  port: process.env.port,
  db_port: process.env.db_port,
  db_host: process.env.db_host,
  db_user: process.env.db_user,
  db_password: process.env.db_password,
  db_name: process.env.db_name,
  jwt_secret: process.env.jwt_secret,
  bcrypt_secret: process.env.bcrypt_secret,
});
