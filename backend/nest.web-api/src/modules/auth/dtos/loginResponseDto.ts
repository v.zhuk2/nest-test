import { UserResponseDto } from "../../user/dtos/UserResponseDto";
import { ApiProperty } from '@nestjs/swagger';


export class LoginResponseDto {
  @ApiProperty()
  accessToken: string;
  @ApiProperty()
  user: UserResponseDto;
}
