import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { UserModule } from '../user/user.module';
import { AuthService } from './auth.service';
import { PasswordEncoder } from './utils/password-encoder.service';
import { JwtModule } from '@nestjs/jwt';
import { TokenModule } from './token/token.module';
import { JwtStrategy } from './strategy/jwt.strategy';
import { RepositoryModule } from '../repository/repository.module';

@Module({
  imports: [JwtModule.register({}), UserModule, TokenModule, RepositoryModule],
  controllers: [AuthController],
  providers: [AuthService, PasswordEncoder, JwtStrategy],
})
export class AuthModule {}
