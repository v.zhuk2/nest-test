import { Body, Controller, Post } from "@nestjs/common";
import { LoginResponseDto } from "./dtos/loginResponseDto";
import { AuthService } from "./auth.service";
import { CreateUserDto } from "../user/dtos/createUserDto";
import { LoginDto } from "./dtos/loginDto";
import { ApiResponse, ApiTags } from "@nestjs/swagger";

@ApiTags("Auth")
@Controller("auth")
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @ApiResponse({ status: 201, type: LoginResponseDto })
  @Post("signUp")
  async signUp(@Body() dto: CreateUserDto): Promise<LoginResponseDto> {
    return await this.authService.signUp(dto);
  }

  @ApiResponse({ status: 200, type: LoginResponseDto })
  @Post("login")
  async login(@Body() dto: LoginDto): Promise<LoginResponseDto> {
    return await this.authService.login(dto);
  }
}
