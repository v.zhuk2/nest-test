import { Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class TokenService {
  constructor(
    private readonly jwt: JwtService,
    private readonly config: ConfigService
  ) {}

  async generateJwt(userId: number, email: string) {
    const payload = {
      sub: userId,
      email,
    };
    const secret = this.config.get("jwt_secret");
    return this.jwt.signAsync(payload, {
      expiresIn: "15m",
      secret: secret,
    });
  }
}
