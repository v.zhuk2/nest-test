import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UserService } from '../user/user.service';
import { CreateUserDto } from '../user/dtos/createUserDto';
import { AppErrors } from '../../common/constants/errors';
import { LoginResponseDto } from './dtos/loginResponseDto';
import { LoginDto } from './dtos/loginDto';
import { PasswordEncoder } from './utils/password-encoder.service';
import { TokenService } from './token/token.service';
import { RepositoryService } from '../repository/repository.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly repository: RepositoryService,
    private readonly userService: UserService,
    private readonly passwordEncoder: PasswordEncoder,
    private readonly tokenService: TokenService,
  ) {}

  async signUp(dto: CreateUserDto): Promise<LoginResponseDto> {
    const userExists = await this.repository.user.findByEmail(dto.email);

    if (userExists) {
      throw new ConflictException(AppErrors.User_Exist);
    }

    const createdUser = await this.userService.createUser(dto);
    return {
      accessToken: await this.tokenService.generateJwt(
        createdUser.id,
        createdUser.email,
      ),
      user: createdUser,
    };
  }

  async login(dto: LoginDto): Promise<LoginResponseDto> {
    const user = await this.repository.user.findByEmail(dto.email);
    if (!user) throw new NotFoundException(AppErrors.User_NotExist);
    if (!(await this.passwordEncoder.compare(dto.password, user.password))) {
      throw new BadRequestException(AppErrors.User_PasswordIncorrect);
    }
    return {
      user: {
        email: user.email,
        name: user.name,
        lastName: user.lastName,
        id: user.id,
      },
      accessToken: await this.tokenService.generateJwt(user.id, user.email),
    };
  }
}
