import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import { RepositoryService } from '../../repository/repository.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    config: ConfigService,
    private readonly repository: RepositoryService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config.get('jwt_secret'),
    });
  }

  async validate(payload: {
    sub: number;
    email: string;
    iat: Date;
    exp: Date;
  }) {
    const user = await this.repository.user.base.findOne({
      where: { id: payload.sub },
      raw: true,
    });
    if (!user) return null;
    return user;
  }
}
