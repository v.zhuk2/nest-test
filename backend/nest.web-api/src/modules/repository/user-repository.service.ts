import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from './models/user.model';
import { CreateUserDto } from '../user/dtos/createUserDto';

@Injectable()
export class UserRepository {
  constructor(@InjectModel(User) private readonly user: typeof User) {}

  async create(dto: CreateUserDto) {
    return await this.user.create({
      email: dto.email,
      password: dto.password,
      name: dto.name,
      lastName: dto.lastName,
    });
  }

  async findByEmail(email: string): Promise<User> {
    return await this.user.findOne({
      where: { email: email },
    });
  }

  get base() {
    return this.user;
  }
}
