import { Module } from '@nestjs/common';
import { UserRepository } from './user-repository.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { User } from './models/user.model';
import { RepositoryService } from './repository.service';

@Module({
  providers: [UserRepository, RepositoryService],
  imports: [SequelizeModule.forFeature([User])],
  exports: [RepositoryService],
})
export class RepositoryModule {}
