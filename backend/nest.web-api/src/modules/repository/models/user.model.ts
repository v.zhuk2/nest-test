import { Column, Model, Table, Unique } from 'sequelize-typescript';

@Table
export class User extends Model {
  @Unique
  @Column
  email: string;

  @Column
  password: string;

  @Column
  name: string;

  @Column
  lastName: string;
}
