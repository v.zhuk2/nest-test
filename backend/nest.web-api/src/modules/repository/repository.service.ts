import { Injectable } from '@nestjs/common';
import { UserRepository } from './user-repository.service';

@Injectable()
export class RepositoryService {
  constructor(private readonly userRepository: UserRepository) {}

  get user(): UserRepository {
    return this.userRepository;
  }
}
