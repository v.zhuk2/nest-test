import { Injectable } from '@nestjs/common';
import { PasswordEncoder } from '../auth/utils/password-encoder.service';
import { CreateUserDto } from './dtos/createUserDto';
import { UserResponseDto } from './dtos/UserResponseDto';
import { RepositoryService } from '../repository/repository.service';

@Injectable()
export class UserService {
  constructor(
    private readonly repository: RepositoryService,
    private readonly passwordEncoder: PasswordEncoder,
  ) {}

  getUsers(): string[] {
    return ['vitkor', 'viktor@gmail.com'];
  }

  async createUser(dto: CreateUserDto): Promise<UserResponseDto> {
    dto.password = await this.passwordEncoder.hash(dto.password);

    const createdUser = await this.repository.user.create(dto);

    return {
      name: createdUser.name,
      lastName: createdUser.lastName,
      email: createdUser.email,
      id: createdUser.id,
    };
  }
}
