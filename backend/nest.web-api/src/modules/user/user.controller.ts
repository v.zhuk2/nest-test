import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dtos/createUserDto';
import { UserResponseDto } from './dtos/UserResponseDto';
import { ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../../guards/jwt-auth-guard';
@UseGuards(JwtAuthGuard)
@ApiTags('Users')
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  getUsers(): string[] {
    return this.userService.getUsers();
  }

  @Post('createUser')
  async createUser(@Body() dto: CreateUserDto): Promise<UserResponseDto> {
    console.log(dto);
    return await this.userService.createUser(dto);
  }
}
