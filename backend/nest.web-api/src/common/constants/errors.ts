export enum AppErrors {
  User_Exist = 'User with this email already exists',
  User_NotExist = 'User with this email does not exists',
  User_PasswordIncorrect = 'Password incorrect', // todo actually need to be number of retries somewhere
}
